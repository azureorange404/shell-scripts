#!/bin/sh

# if ! [ "$(ls -l --time-style=+%d.%m.%y .cache/word-of-the-day | awk -F' ' '{ print $6 }')" = "$(date +%d.%m.%y)" ]; then
while read -r entry; do
    # entry="$(shuf "$HOME"/.local/source/scripts/daily-word.txt | shuf | head -n1)"
    word="$(printf "%s" "$entry" | cut -d] -f1 | sed -e 's/\[//')"
cat << EOF
$(printf "\n%s\n" "$entry")
$(trans "$word")
-----
EOF

# trans "$word" > "$HOME"/.cache/word-of-the-day
# printf "\n%s\n" "$entry" >> "$HOME"/.cache/word-of-the-day
# fi
done < "$HOME/.local/source/scripts/daily-word.txt"

# tail -n+3 "$HOME"/.cache/word-of-the-day 
