#!/bin/bash

if ! [ "$(ls -l --time-style=+%d.%m.%y "$HOME/.cache/word-of-the-day" | awk -F' ' '{ print $6 }')" = "$(date +%d.%m.%y)" ]; then
    cat "$HOME/.cache/daily-word/$(ls "$HOME/.cache/daily-word/" | shuf | head -n1)" > "$HOME/.cache/word-of-the-day"
fi

cat "$HOME/.cache/word-of-the-day"
