# Table of Contents

1. [Shell Scripts](#shell-scripts)
2. [Important](#important)
3. [Contributions](#contributions)
4. [Dependencies](#dependencies)
5. [Installation](#installation)

# Shell Scripts

I collected many shell scripts during my time using Linux and also wrote quite a few myself.
Not all the scripts in this collection where written by me and I try to mark their origin somewhere.

This collection includes scripts for:

- [ffmpeg](./ffmpeg) (edit video)
- [imagemagick](./magick) (edit pictures)
- latex
- dunst
- some screen lockers
- dwm (I'm on wayland now)
- xmobar (same)
- waybar
- and much more garbage to be found

My dmenu scripts can be found in [this repo](https://gitlab.com/azureorangexyz/dmenu-scripts).

# IMPORTANT

Many scripts weren't used in quite some time and they may be faulty.
Make shure to try them in a safe environment first!

But none of the scripts should be harmful as I used all of them at least once.
They may be deprecated though.

# Contributions

I'd be happy to receive issues, pull requests and as such contributions to this collection.
It is nowhere near organized but it will improve over time.

## script structure

In the beginning of each script there should be a description header telling generic info about the script:

```sh
#!/bin/sh

# Script name:  ff-script
# Description:  An example script for demonstrating the general structure
# Dependencies: ffmpeg, GNU Core Utils
# GitLab:       https://gitlab.com/azureorangexyz/shell-scripts/
# License:      https://gitlab.com/azureorangexyz/shell-scripts/-/blob/main/LICENSE
# Contributors: azureorangexyz
                and anyone who is willing to help me out
                or the ones I got the scripts from in the first place
```

I try my best to keep scripts POSIX compliant, hence the #!/bin/sh.

Then there should be a section where variables are defined:

```sh
VAR1="var1"
VAR2="var2"
VAR3="var3"
VAR4="var4"
```

Then the generic help output is defined in a seperate function, which may be called under various conditions:

```sh
help_function() {
echo "
$(basename "$0") [OPTIONS] [input-file]

OPTIONS:

-h              display this help message
...
"
}
```

Using the `getopts` function, options to the script are read and arguments get processed.
The second part here searches for the last argument in the list and assigns it to $input_file:

```sh
while getopts "hv:" arg; do case "${arg}" in
    h) help_function && exit 0 ;;
    r) VAR1="$OPTARG" ;;
esac done

for i in "$@"; do :; done
if [ -f "$i" ]; then
    input_file="$i"
else
    echo "No input file specified." && help_function && exit 1
fi
```

After this the main script follows.
I see it as a general rule of thumb to clean up any mess after the main task finished.

# Dependencies

Many scripts have different dependencies and I tried to mention them in the top comment block
of each script. Otherwise you'll find out if you try to run them. ;)

# Installation

I recommend to clone this repo using git:

```sh
git clone https://gitlab.com/azureorangexyz/shell-scripts.git
```

Then add symlinks for the scripts you would like to use:

```sh
ln -s /home/$USER/<PATH TO THE SCRIPT> /home/$USER/.local/bin/
```

I plan to add an installation file that does this automatically in the future.
