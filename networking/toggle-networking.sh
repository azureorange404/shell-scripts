#!/bin/sh

if nmcli n | grep -q "disabled" ; then
    nmcli n on
    notify-send -i network-wireless-full "Wireless enabled" "Your wireless adaptor has been enabled."
else
    nmcli n off
    notify-send -i network-wireless-disconnected "Wireless disabled" "Your wireless adaptor has been disabled."
fi
