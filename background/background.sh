#!/bin/bash

if [ $# -eq 0 ]; then
    # Set random background:
    feh --randomize --bg-fill ~/.wallpapers/*
else
    _type=$(xdg-mime query filetype $1 | cut -d "/" -f 1)
    ! [[ "$_type" = "image" ]] && notify-send "File type error" "The selected file $1 is of the file type $_type, but it needs to be an image." && exit 0
    feh --bg-fill $1
fi

# Get background path:
_current=$(cat ~/.fehbg | awk '{print $4}' | sed -e s/\'//g )

# Set lockscreen background

betterlockscreen -u $_current && notify-send "Background" "The lockscreen background has been updated." &
