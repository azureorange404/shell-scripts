#!/bin/sh

if pgrep -x "trayer" > /dev/null
then
    killall trayer
else
    trayer --edge top --widthtype request --heighttype request --padding 8 --distance 20 --iconspacing 5 --SetDockType false --SetPartialStrut true --expand true --monitor primary --tint 0x202020 &

    sleep 0.2
    pid=$(xdotool search --name "panel")
    geo=$(xdotool getwindowgeometry "$pid")
    xpos=$(echo "$geo" | cut -d ' ' -f 4 | cut -d ',' -f 1)
    ypos=$(echo "$geo" | cut -d ' ' -f 4 | cut -d ',' -f 2)
    xgeo=$(echo "$geo" | cut -d ' ' -f 8 | cut -d 'x' -f 1)
    ygeo=$(echo "$geo" | cut -d ' ' -f 8 | cut -d 'x' -f 2)
    
    echo "$xgeo"

    x=$(( xpos + ( xgeo / 2 ) ))
    y=$(( ypos + ( ygeo / 2 ) ))
    xdotool mousemove $x $y
fi
