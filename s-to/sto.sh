#!/bin/sh

baseurl="https://s.to"
basepath="$HOME/.local/share/serien-stream"
alphabet="$HOME/.local/source/scripts/s-to/serien-alphabet.html"
genres="$HOME/.local/source/scripts/s-to/serien-genres.html"

while getopts "hd" o; do case "${o}" in
    h)usage;;
    d)dmenu=true;;
    *)usage;;
esac done

usage() {
    echo "usage"
}

serie="$(grep "<li><a data-alternative-titles=\".*\" href=\"/serie/.*\" title=" "$alphabet" | \
    sed -e 's/<li><a data-alternative-titles=\".*\" href=\"\/serie\/.*\" title="\(.*\) Stream anschauen\">.*<\/a><\/li>/\1/' \
    -e 's/^\s*//' | dmenu -i -c -l 20)"

url="$(grep "$serie" "$alphabet" | \
    sed -e 's/<li><a data-alternative-titles=\".*\" href=\"\(.*\)\" title=\".* Stream anschauen\">.*<\/a><\/li>/\1/')"

file="$(echo "$url" | sed -e 's/^\/serie\/stream\///').html"

[ -d "$basepath" ] || mkdir "$basepath"
[ -f "$basepath/$file" ] || curl "$baseurl/$url" > "$basepath/$file"

seasons="$(sed -e '0,/<li><span><strong>Staffeln\:<\/strong><\/span><\/li>/d' \
    -e '1,/<\/ul>/!d' \
    -e 's/class="active" //' \
    -e 's/ <\/ul>//' -e '/^$/d' -e 's/^\s*//' "$basepath/$file")"

season="$(echo "$seasons" | sed -e 's/<li>  <a href="\/serie\/stream\/.*" title="\(.*\)">.*<\/a>  <\/li>/\1/' \
    -e 's/\s*$//' | dmenu -i -c -l 20)"

url="$(echo "$seasons" | grep "$season" | sed -e 's/^<li>  <a href="\(.*\)" title=".*">.*<\/a>  <\/li>/\1/' \
    -e 's/\s*$//')"
file="$(echo "$url" | sed -e 's/^\/serie\/stream\///' -e 's/\//\-/').html"

[ -f "$basepath/$file" ] || curl "$baseurl$url" > "$basepath/$file"

episodes="$(curl -s "https://s.to/serie/stream/solar-opposites/staffel-4" | \
    sed -e 's/<\/li>/<\/li>\n/' | \
    grep "data-episode-id" | \
    sed -e '/^$/d' -e 's/^\s*//' \
    -e '1,/<tbody id/!d;/<tbody id/d' \
    -e 's/\s*$//')"

episode="$(echo "$episodes" | \
    sed -e 's/<li>\s*<a\s*href="\/serie\/stream\/.*" data-episode-id="[0-9]*" title="//g' \
    -e 's/^Staffel [0-9]* //' \
    -e 's/"$//' | \
    dmenu -i -c -l 20)"

echo "now it fails" && exit

hoster="$(grep "$episode/des.* class=\"hoster" "$file" | \
    sed -e 's/^.*title="//' -e 's/\"><i\ class=\"hoster.*$//' | \
    dmenu -i -c -l 20)"

echo "$hoster"

