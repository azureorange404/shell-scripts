#!/bin/sh

while true
do
    _app=$(newm-cmd debug | grep focused=True | cut -d "," -f 2 | sed 's/\ //g')
    _title=$(newm-cmd debug | grep focused=True | cut -d "," -f 1 | cut -d ":" -f 3)
    echo "$_title"
    sleep 0.25
done
