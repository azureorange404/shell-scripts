#!/bin/sh

_amount=$(checkupdates | wc -l)

if ! [ "$_amount" -eq 0 ]
then
    if [ "$_amount" -gt 100 ]
    then
        echo "{\"text\": \"$_amount updates\", \"class\": \"high\"}"
    elif [ "$_amount" -gt 50 ]
    then
        echo "{\"text\": \"$_amount updates\", \"class\": \"middle\"}"
    else
        echo "{\"text\": \"$_amount updates\", \"class\": \"low\"}"
    fi
fi
