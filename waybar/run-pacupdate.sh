#!/bin/sh

checkupdates

printf "\n%s\n" "Update your system? (yes/no)"

read -r ans

if [ "$ans" = "yes" ]
then
    sudo pacman -Syu
else
    exit 1
fi
