cat "$1" | trans -show-original n \
        -show-original-phonetics n \
        -show-translation-phonetics n \
        -show-prompt-message n \
        -show-languages n \
        -show-original-dictionary n \
        -show-dictionary n \
        -show-alternatives n :no :en > "$2"
