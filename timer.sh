#!/bin/sh

if [ -n "$1" ]; then
    time=$1
else
    printf "Enter time in seconds: "
    read -r time
fi

cleanup() {
    tput cnorm
}

trap cleanup EXIT

tput civis

convert() {

    if [ "$time" -lt 60 ]; then
        printf "%ss" "$time"
    elif [ "$time" -lt 3600 ]; then
        date -d@"$time" -u "+%Mm %Ss"
    else
        date -d@"$time" -u "+%Hh %Mm %Ss"
    fi

}

while [ "$time" -gt 0 ]
do
    clear
    echo ""
    figlet -m-1 "$(convert)"
    time=$(( time - 1 ))
    sleep 1
done

[ -f "$HOME/.config/sounds/beep.mp3" ] && paplay "$HOME/.config/sounds/beep.mp3" &
clear
echo ""
figlet -m-1 "$(convert)"
time=$(( time - 1 ))
sleep 1

tput cnorm

