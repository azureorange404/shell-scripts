# ffmpeg

## scripts

- [ff-credits](#ff-credits)
- [ff-create](#ff-create)
- [ff-camcap](#ff-camcap)
- ff-bgaudio
- ff-camview
- ff-cat
- ff-catcut
- ff-concat
- ff-decimate (from)
- ff-fixaudio (from)
- ff-frozen-frames-detect (from)
- ff-noise
- ff-noiseclean
- ff-res.sh
- ff-screencap
- ff-silence-detect
- ff-silence-trim
- ff-slider
- ff-timelaps
- ff-video2gif

### ff-credits

This is an ffmpeg script that reads movie credits from a text file and converts them to a video file.

It uses some simple math to calculate the length of the video from the line count of the input file.
I don't know if this always works as I did not find much use for the script.

Also you have to fiddle with the formatting to get it right.

There is an example input file [here](./examples/ff-credits/input.txt) and this is what the result looks like (I rendered it down to save disk space):

![example-credits](./examples/ff-credits/output.mp4)

The script still has some issues:
- the credits are not always centered
- the speed cannot be adjusted
- the last three lines were cut off
- the calculations are somewhat off

### ff-create

This script takes a set of input video clips specified in a text file and concatenates them to one video file.
Between the clips random transitions are inserted by default (this can be disabled by using the `-0` flag).

### ff-camcap

A simple script for capturing your webcam.
All necessary settings can be adjusted by using the appropriate option to the script.

The help function tries to output useful information to your webcam, so check it out.

The script captures audio as well (can be disabled), but this part needs some more customization options.

