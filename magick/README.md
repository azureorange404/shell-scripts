# imagemagick

## scripts

- [auto-wallpaper](#auto-wallpaper)

### auto-wallpaper

auto-wallpaper is a script that takes an image from the wallpaper directory and applies some effects:

- oil painting style
- a random quote
- a calender with the current day in red

It also trims the chosen picture to your screen resolution (mainly for the correct placement of the stuff above).
Then it sets the wallpaper using swww.
