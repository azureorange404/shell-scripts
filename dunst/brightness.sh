#!/bin/sh

# You can call this script like this:
# $./brightness.sh up
# $./brightness.sh down

DIR="$HOME/$(dirname "$0")"

get_brightness () {
    echo $((100*$(brightnessctl g)/$(brightnessctl m)))
}

send_notification () {
    brightness=`get_brightness`
    bar=$(seq -s "─" $(($brightness / 5)) | sed 's/[0-9]//g')
    # Send the notification
    if [ $brightness -lt 45 ]; then
			_icon="󰃞 "
    elif [ $brightness -lt 75 ]; then
			_icon="󰃟 "
    else
			_icon="󰃠 "
    fi
    dunstify -t 800 -r 2593 -u low "$_icon $brightness    $bar"
}

case $1 in
    up)
	# Up the volume (+ 5%)
	brightnessctl s -n 5%+ > /dev/null
	send_notification
	;;
    down)
	brightnessctl s -n 5%- > /dev/null
	send_notification
	;;
esac

sigdwmblocks 6
