#!/bin/sh

# You can call this script like this:
# $./volume.sh up
# $./volume.sh down
# $./volume.sh mute

DIR="$HOME/$(dirname "$0")"

get_volume () {
    # amixer get Master | grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1
	pactl get-sink-volume 0 | grep '%' | cut -d '/' -f 2 | cut -d '%' -f 1 | sed -e s/\ //g
}

is_mute () {
    # amixer get Master | grep '%' | grep -oE '[^ ]+$' | grep off > /dev/null
	pactl get-sink-mute 0 | cut -d ':' -f 2 | sed -e s/\ //g | grep yes > /dev/null
}

send_notification () {
    volume=`get_volume`
    # Make the bar with the special character ─ (it's not dash -)
    # https://en.wikipedia.org/wiki/Box-drawing_character
    bar=$(seq -s "─" $(($volume / 5)) | sed 's/[0-9]//g')
    # Send the notification
	if [ $volume -eq 0 ]; then
			_icon="󰝟 "
    elif [ $volume -lt 30 ]; then
			_icon="󰕿 "
    elif [ $volume -lt 70 ]; then
			_icon="󰖀 "
    else
			_icon="󰕾 "
    fi
    dunstify -t 800 -r 2593 -u low "$_icon $volume    $bar"
}

case $1 in
    up)
	# Set the volume on (if it was muted)
	pactl set-sink-mute 0 off > /dev/null
	# Up the volume (+ 5%)
	pactl set-sink-volume 0 +5% > /dev/null
	send_notification
	;;
    down)
    pactl set-sink-mute 0 off > /dev/null
	pactl set-sink-volume 0 -5% > /dev/null
	send_notification
	;;
    mute)
    	# Toggle mute
	pactl set-sink-mute 0 toggle > /dev/null
	if is_mute ; then
	    dunstify -i "$DIR/mute.svg" -t 800 -r 2593 -u low "Mute"
	else
	    send_notification
	fi
	;;
esac

sigdwmblocks 5
