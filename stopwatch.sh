#!/bin/sh

cleanup() {
    tput cnorm
}

trap cleanup EXIT

tput civis

time=0

convert() {

    if [ "$time" -lt 60 ]; then
        printf "%ss" "$time"
    elif [ "$time" -lt 3600 ]; then
        date -d@"$time" -u "+%Mm %Ss"
    else
        date -d@"$time" -u "+%Hh %Mm %Ss"
    fi

}

playsound() {

    [ -f "$HOME/.config/sounds/start.mp3" ] && \
        [ "$time" -gt 0 ] && \
        [ $(( time % 60)) -eq 0 ] && \
        paplay "$HOME/.config/sounds/start.mp3" &

}

while true
do
    playsound
    clear
    echo ""
    figlet -m-1 "$(convert)"
    time=$(( time + 1 ))
    sleep 1
done

tput cnorm
