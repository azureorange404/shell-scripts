#!/bin/sh

file="$1"
file_type="$(file --mime-type "$file" | cut -d' ' -f2)"
basename="$(basename "$file")"

case "$file_type" in
    text/x-tex)
        content="$(pandoc "$file" -f latex -t plain | tr '\n' ' ')"
        chars="$(echo "$content" | sed 's/\s//g' | wc -m)"
        words="$(echo "$content" | wc -w)"
        echo "$content" | sed 's/\s//g'
        ;;
    *)
        content="$(tr '\n' ' ' < "$file")"
        chars="$(echo "$content" | sed 's/\s//g' | wc -m)"
        words="$(echo "$content" | wc -w)"
        ;;
esac

printf "%s\n\n%s\n%s\n" \
    "$basename: $file_type" \
    "$chars characters" \
    "$words words"
notify-send "$basename: $file_type" "$chars characters\n$words words"
