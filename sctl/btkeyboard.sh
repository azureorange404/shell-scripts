#!/bin/bash

# Toggle connection to bluetooth device

mac="EC:5F:46:46:EE:9F"

if bluetoothctl info "$mac" | grep -q 'Connected: yes'; then
    echo "$mac already connected"
    exit 1
    #echo "Turning off $mac"
    #bluetoothctl disconnect || echo "Error $?"
else
    echo "Turning on $mac"
    # turn on bluetooth in case it's off
    #rfkill unblock bluetooth

    bluetoothctl power on
    bluetoothctl connect "$mac"
    #sink=$(pactl list short sinks | grep bluez | awk '{print $2}')

    #if [ -n "$sink" ]; then
    #    pacmd set-default-sink "$sink" && echo "OK default sink : $sink"
    #else
    #    echo could not find bluetooth sink
    #    exit 1
    #fi
fi
