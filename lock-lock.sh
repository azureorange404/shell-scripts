#!/bin/sh

LOCK_FILE="$HOME/.local/share/lockstate"
LOCK_STATE="$(cat "$LOCK_FILE")"


if [ "$LOCK_STATE" = "enabled" ]; then
    echo "disabled" > "$LOCK_FILE"
    notify-send "autolock" "disabled"
else
    echo "enabled" > "$LOCK_FILE"
    notify-send "autolock" "enabled"
fi

if pgrep dwmblocks >/dev/null; then
    sigdwmblocks 2
fi
