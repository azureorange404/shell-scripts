#!/bin/sh


abort() {
    notify-send "qr-reader" "qr-code scan failed"
    pkill "$(basename "$0")"
}

missing() {
    echo "$1 is not installed
    
On Arch run:
# pacman -S $1
"
    exit 1
}

if ! command -v zbarimg >/dev/null; then missing "zbar"; fi

case $1 in
    gui)
        if ! command -v flameshot >/dev/null; then missing "flameshot"; fi
        flameshot gui --raw | zbarimg -q - || abort
        ;;
    cam)
        timeout 30 zbarcam --quiet --oneshot || abort
        ;;
    screen|*)
        if ! command -v import >/dev/null; then missing "imagemagick"; fi
        import -silent -window root bmp:- | zbarimg -q - || abort
        ;;
esac | grep "QR-Code" | sed -e 's/^QR-Code://' | xclip -r -selection clipboard

notify-send "qr-reader" "Contents copied to clipboard"
