#!/bin/sh

# This script compiles an ms groff document to a pdf
# But beware it only works if "ble of Contents" is not a term used in the Document
# apart from the Table of Contents itself of course
# AND
# only if the abstract does not need two pages (what it shouldn't anyway)

file="$1"
base="${file%.*}"

# check to see if the file generates a table of contents
# and directly compile pdf if not
if [ "$(grep -i '.TC' "$file")" = "" ]; then
    preconv "$file" | refer -PS -e | groff -me -ms -kept -T pdf > "$base".pdf
    exit 0
fi

# compile postscript document
groff -m ms -k -Tps "$file" > "$base".ps

csplit --prefix=/tmp/TOC example.ps /ble\ of\ Contents/-5 # --> TOC00, TOC01
csplit --prefix=/tmp/CONT /tmp/TOC00 /^%%Page:\ 1\ 2/ # --> CONT00, CONT01
csplit --prefix=/tmp/TOC /tmp/TOC01 /^%%Trailer/ # --> TOC00, TOC01

# compile pdf
cat /tmp/CONT00 /tmp/TOC00 /tmp/CONT01 /tmp/TOC01 | ps2pdf - "$base".pdf

# cleanup
rm "$base".ps
rm /tmp/TOC*
rm /tmp/CONT*
