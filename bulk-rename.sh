#!/bin/sh

for i in "$@"; do
    if ! [ -f "$i" ] && ! [ -d "$i" ]; then
        echo "$i is not a file"
        exit 1
    fi
done

qmv \
    -e 'nvim \
        -c "silent !cat % > /tmp/bulkrename-tempfile" \
        -c "bo vs|view /tmp/bulkrename-tempfile" \
        -c "wincmd w" \
        -c "cnoreabbrev q qa" \
        -c "cnoreabbrev wq wqa"' \
    -f destination-only "$@"

[ -f "/tmp/bulkrename-tempfile" ] && rm "/tmp/bulkrename-tempfile"
