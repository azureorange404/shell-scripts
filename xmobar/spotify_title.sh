#!/bin/bash

ten="          " 

if ! sp current | grep "Error: " ; then

	#_title=$(printf "$(sp current | grep Title | sed -e 's/Title//' -e 's/^[ \\t]*//') - ")
	_title=$(sp current | grep Title | sed -e 's/Title//' -e 's/^[ \\t]*//')

for (( c=1; c<=$((${#_title})); c++ ))
do
	
	_string=$(echo $_title | cut -c ${c}- | cut -c -12)
	_string="${_string:0:10}${ten:0:$((10 - ${#_string}))}"
	echo "${_string}" >/tmp/spotify_title.txt
	
	sleep 1
done

else

		echo "" >/tmp/spotify_title.txt
		sleep 5

fi

if pidof spotify ; then
	bash /home/azure/.config/shell/spotify_title.sh
else
	echo "" >/tmp/spotify_title.txt && exit
fi
