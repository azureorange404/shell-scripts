#!/bin/bash

_numlock=$(xset q | grep 01 | sed 's/ \+/:/g' | cut -d ':' -f 13)


if ! [[ $_numlock = "on" ]]; then
		echo '  🔒'
fi
