#!/bin/sh

_head="Biggest memory hogs:"
_hogs=$(ps axch -o cmd:50,%mem --sort -%mem | head)

msg=$(printf "$_head\n$_hogs")

notify-send -t 10000 -u low "$msg"
