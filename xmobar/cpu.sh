#!/bin/sh

_head="Biggest cpu hogs:"
_hogs=$(ps axch -o cmd:50,%cpu --sort -%cpu | head)

msg=$(printf "$_head\n$_hogs")

notify-send -t 10000 -u low "$msg"
