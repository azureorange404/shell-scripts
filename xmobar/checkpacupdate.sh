#!/bin/bash

_updates=$(checkupdates | wc -l)

if [[ $_updates -eq 0 ]]; then
		notify-send "There are currently no updates available."
else
		notify-send "Updates Available:" "$(checkupdates)"
fi
