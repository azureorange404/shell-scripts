#!/bin/sh

_head="File System usage:"
_fsu=$(lsblk -o NAME,LABEL,FSAVAIL,SIZE,FSUSE%)

msg=$(printf ${_fsu})

notify-send -t 10000 -u low "$_fsu"
