#!/bin/bash

_sink=$(pactl info | grep "Default Sink:" | cut -d ':' -f 2 | sed 's/\ //g')

# _status=$(amixer get Master | grep 'Front Left:' | cut -d '[' -f 3 | sed 's/]//')
_status=$(pactl get-sink-mute $_sink | cut -d ':' -f 2 | sed 's/\ //g')

# _volume=$(amixer get Master | grep 'Front Left:' | cut -d '[' -f 2 |     sed 's/]//')
_volume=$(pactl get-sink-volume $_sink | grep Volume | cut -d '/' -f 2 | sed 's/\ //g')

if [ $_status = "no" ]; then
	echo $_volume
else
    echo "mute"
fi
