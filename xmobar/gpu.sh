#!/bin/bash

nvidia-smi | grep 'MiB /' | cut -d '|' -f 3 | cut -d '/' -f 1 | sed s/\ //g
