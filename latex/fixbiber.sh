#!/bin/sh

case $1 in
    *.tex)
	    pdflatex "$1"
        biber "${1%%*(.tex)}"                                                       
        pdflatex "$1"
        pdflatex "$1"
        ;;
    *)
	    printf "Exit with error 69"
        ;;
esac
