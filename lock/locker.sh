#!/bin/sh
#exec xautolock -exit

exec xautolock -corners 0-00 -cornersize 20 -detectsleep \
  -time 3 -locker "$HOME/.config/shell/scripts/lock/lock -p -c" \
  -notify 30 \
  -notifier "notify-send -h int:transient:1 -u critical -t 10000 -- 'LOCKING screen in 30 seconds'"

# exec xautolock -corners 0-00 -cornersize 20 -detectsleep \
  # -time 3 -locker "[ $[$RANDOM % 2] = 0 ] && slock || i3lock -nbfc 000000" \
  # -notify 30 \
  # -notifier "notify-send -h int:transient:1 -u critical -t 10000 -- 'LOCKING screen in 30 seconds'"
