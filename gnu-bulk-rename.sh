#!/bin/sh

early_exit() {
    echo "fuck you"
    /usr/bin/rm "$TMP_FILE_1" "$TMP_FILE_2" "$LOG_FILE_1" "$LOG_FILE_2" "$LOG_FILE_3"
    exit 1
}

TMP_FILE_1="/tmp/bulk-rename-$(tr -dc A-Za-z0-9 </dev/urandom | head -c 6 ; echo '')"
TMP_FILE_2="/tmp/bulk-rename-$(tr -dc A-Za-z0-9 </dev/urandom | head -c 6 ; echo '')"
LOG_FILE_1="/tmp/bulk-rename-$(tr -dc A-Za-z0-9 </dev/urandom | head -c 6 ; echo '')"
LOG_FILE_2="/tmp/bulk-rename-$(tr -dc A-Za-z0-9 </dev/urandom | head -c 6 ; echo '')"
LOG_FILE_3="/tmp/bulk-rename-$(tr -dc A-Za-z0-9 </dev/urandom | head -c 6 ; echo '')"

printf "%s\n" "$@" | tee "$TMP_FILE_1" "$TMP_FILE_2" "$LOG_FILE_1" "$LOG_FILE_2" "$LOG_FILE_3" >/dev/null

nvim "$TMP_FILE_1" \
        -c "bo vs|view $TMP_FILE_2" \
        -c "wincmd w" \
        -c "cnoreabbrev q qa" \
        -c "cnoreabbrev wq wqa"

if [ "$(diff "$TMP_FILE_1" "$LOG_FILE_1")" = "" ]
then
    echo "Nothing changed."
    early_exit
fi

empty_lines="$(grep -n "^$" "$TMP_FILE_1" | cut -d: -f1)"

if [ "$(wc -l "$TMP_FILE_1" | cut -d' ' -f1)" -lt "$(wc -l "$LOG_FILE_1" | cut -d' ' -f1)" ]
then
    diff "$TMP_FILE_1" "$LOG_FILE_1" | grep ">" | cut -d' ' -f2 | while read -r missing_line
    do
        echo "bulk-rename: New filename for $missing_line was removed."
    done
    early_exit
elif [ "$(wc -l "$TMP_FILE_1" | cut -d' ' -f1)" -gt "$(wc -l "$LOG_FILE_1" | cut -d' ' -f1)" ]
then
    diff "$TMP_FILE_1" "$LOG_FILE_1" | grep "<" | cut -d' ' -f2 | while read -r mysterious_line
    do
        echo "bulk-rename: You have added a mysterious line: $mysterious_line"
    done
    early_exit
elif ! [ "$empty_lines" = "" ]
then
    echo "$empty_lines" | while read -r empty_line; do
        echo "bulk-rename: new filename for $(head -"$empty_line" < "$LOG_FILE_1" | tail -1) is empty."
    done
    early_exit
fi

COUNT=1
while read -r line <&9; do
    dest="$(head -"$COUNT" < "$TMP_FILE_1" | tail -1)"
    if ! [ "$line" = "$dest" ]
    then
        if grep "$dest" "$LOG_FILE_2" >/dev/null; then
            newdest="$line-$(tr -dc A-Za-z0-9 </dev/urandom | head -c 10 ; echo '')"
            echo "$newdest^$dest" >> "$LOG_FILE_3"
            dest="$newdest"
        fi
        /usr/bin/mv -iv "$line" "$dest"
    fi
    COUNT=$(( COUNT + 1 ))
done 9< "$LOG_FILE_1"

diff "$LOG_FILE_1" "$LOG_FILE_3" | grep ">" | cut -d' ' -f2 | while read -r temporary_file
do
    mv -iv "$(echo "$temporary_file" | cut -d'^' -f1)" "$(echo "$temporary_file" | cut -d'^' -f2)"
done

/usr/bin/rm "$TMP_FILE_1" "$TMP_FILE_2" "$LOG_FILE_1" "$LOG_FILE_2" "$LOG_FILE_3"
