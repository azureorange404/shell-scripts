#!/bin/sh

CONFIG="$HOME/.config/homepage.conf"
BOOKMARKS="$HOME/.local/share/bookmarks.txt"
HOMEPAGE="$HOME/.local/share/homepage.html"
search_url="https://search.brave.com/search?q="

[ -f "$CONFIG" ] && source "$CONFIG"

URLS="$(grep 'http' "$BOOKMARKS" | grep -o '[^ ]*$')"

echo "Processing bookmarks:"
count="$(echo "$URLS" | wc -l)"
i=1
BUTTONS=""
for line in $URLS; do
    PERCENTAGE="$((i * 100 / count))"
    printf '\n\e[1A\e[1A%s\n[' "$line"
    printf -- '-%.0s' $(seq $((PERCENTAGE / 2)))
    printf -- ' %.0s' $(seq $((50 - (PERCENTAGE / 2))))
    printf '%s' "] $PERCENTAGE%"
    i=$((i + 1))
    TITLE="$(timeout 10 wget -qO- "$line" |
        perl -l -0777 -ne 'print $1 if /<title.*?>\s*(.*?)\s*<\/title/si')"
    if [ "$TITLE" = "" ]; then
        TITLE="$(grep "$line" "$BOOKMARKS" | sed 's/^\(.*\)\shttp.*$/\1/')"
    fi

    BUTTONS="
    $BUTTONS
    $(printf "<li class=\"blue-btn\"><a class=\"first-link\" href=\"%s\">$TITLE</a></li>\n" "$line")
    "
done

cat > "$HOMEPAGE" <<EOF
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homepage</title>
    <link rel="stylesheet" href="./style.css">
    <link rel="icon" href="./favicon.ico" type="image/x-icon">
    <style>
      h1 {text-align: center;}
      div {
          text-align: center;
          margin-right: 15%;
          margin-left: 15%;
      }
      #myInput {
        background-color: #313244;
        color: #cdd6f4;
        width: 50%;
        font-size: 16px;
        padding: 12px 20px 12px 12px;
        border: 1px solid #ddd;
        border-radius: 12px;
        margin-bottom: 12px;
      }
      .search-btn {
        background-color: #313244;
        color: #cdd6f4;
        font-size: 16px;
        padding: 12px 20px 12px 12px;
        border: 1px solid #ddd;
        border-radius: 12px;
        margin-bottom: 12px;
        margin-left: 12px;
      }
      ::placeholder { color: #cdd6f4; opacity: 0.5; }
      .blue-btn a{
        color: #cdd6f4;
        text-decoration:none;
        margin-top: 0em;
        text-align: center;
        display: flow-root; /* important */
        white-space: nowrap;
        overflow: clip;
        text-overflow: fade;
      }
      
      .blue-btn, .first-link:hover{
        -webkit-transition: 3.3s;
        -moz-transition: 3.3s;
        transition: 3.3s;     
        
        -webkit-transition-timing-function: linear;
        -moz-transition-timing-function: linear;
        transition-timing-function: linear;
      }
      
      .blue-btn{
        outline: 1px solid #cdd6f4;
        color: black;
        background-color: #313244;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        margin: 8px 4px;
        cursor: pointer;
        height: 20px;
        font-size: 18px;
        border-radius: 12px;
        overflow: clip;
        width: 300px;
      }
      
      .first-link{
        margin-left: 0em;
      }
      
      .first-link:hover{
        margin-left: -300px;
      }
    </style>
    <script>
      url = "$search_url";
      function filterFunction() {
        // Declare variables
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById('myInput');
        filter = input.value.toUpperCase();
        ul = document.getElementById("myUL");
        li = ul.getElementsByTagName('li');

          query = document.getElementById('search-query');
          query.getElementsByTagName("a")[0].innerText = input.value;
          query.getElementsByTagName("a")[0].href = url+input.value;

        // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < li.length; i++) {
          a = li[i].getElementsByTagName("a")[0];
          txtValue = a.textContent || a.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
          } else {
            li[i].style.display = "none";
          }
        }
      }
      function searchFunction() {
        ul = document.getElementById("myUL");
        li = ul.getElementsByTagName('li');

        for (i = 0; i < li.length; i++) {
            if (li[i].style.display != "none") {
                window.open(li[i].getElementsByTagName('a')[0].href,"_self");
                return;
            }
        }
        search = document.getElementById('myInput').value;
        window.open(url+search,"_self");
      }
      function search() {
        if(event.key === 'Enter') {
          searchFunction()
        }
      }
    </script>
  </head>
  <body style="background-color:#313244;">
    <main>
        <h1 style="color:#cdd6f4;">Welcome Aaron</h1>  
        <div>
            <input type="text" id="myInput" onkeyup="filterFunction()" onkeydown="search()" placeholder="Search bookmarks / web"><button class="search-btn" onclick="searchFunction()">Search</button>
        </div>
        <div>
        <ul id="myUL">
        $BUTTONS
        </ul>
        <ul>
            <li id="search-query" class="blue-btn"><a class="first-link"></a></li>
        </ul>
        </div>
    </main>
	<script src="index.js"></script>
  </body>
</html>
EOF
