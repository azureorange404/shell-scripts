#!/usr/bin/env bash
set -euo pipefail

#length=$(expr length ${string[i]})

#stringlength () {
#	read stringg
#	for i in "${stringg[@]}"
#	do
		#if [[ $length -lt 5 ]]; then
		#	sed ''
		#fi
#	done
#}

sed -n '/START_KEYS/,/END_KEYS/p' ~/.xmonad/xmonad.hs | \
    grep -e ', ("' \
    -e '\[ (' \
    -e 'KB_GROUP' | \
    grep -v '\-\- , ("' | \
    grep -v '\-\-, ("' | \
    sed -e 's/^[ \t]*//' \
    -e 's/, (/(/' \
    -e 's/\[ (/(/' \
    -e 's/-- KB_GROUP /\n/' \
    -e 's/", /\t\t\t: /' | \
	sed -e 's/("/ /' \
	-e 's/M-/Mod-/' \
	-e 's/-C-/-Ctrl-/' \
	-e 's/-S-/-Shift-/' | \
	sed -e 's/spawn "\([^:]*\).*")//' \
	-e 's/spawn (\([^:]*\).*))//' | \
	sed -e 's/ \+\-\- /\t/' >> keys.txt
