#!/bin/bash

printf "\
		\n\n\n
        LF KEYBINDINGS

        rename

            rr \t\t : \t\t rename
            rR \t\t : \t\t autorename
            cc \t\t : \t\t new rename
            i \t\t : \t\t rename before extension
            a \t\t : \t\t rename after extension
            I \t\t : \t\t rename at very beginning
            A \t\t : \t\t rename at very end
            B \t\t : \t\t bulk rename

        change directory

            cd \t\t : \t\t cd (prompt)
            gg \t\t : \t\t go to top
            gh \t\t : \t\t cd ~
            gD \t\t : \t\t cd ~/Downloads
            gd \t\t : \t\t cd ~/Documents
            gp \t\t : \t\t cd ~/Documents/phsg
            gP \t\t : \t\t cd ~/Pictures
            gs \t\t : \t\t cd ~/Pictures/screenshots
            gt \t\t : \t\t cd ~/.local/share/Trash/files
            gc \t\t : \t\t cd ~/.config
            gw \t\t : \t\t cd ~/.wallpapers
            gu \t\t : \t\t cd /run/media/azure

            mm \t\t : \t\t mark-save [+ key]
            ml \t\t : \t\t mark-load [+ key]
            mr \t\t : \t\t mark-remove [+ key]

        file operations

            <delete> \t : \t\t cut
            dd \t\t : \t\t cut
            dD \t\t : \t\t trash
            dT \t\t : \t\t empty trash
            rT \t\t : \t\t restore trash
            yy \t\t : \t\t copy
            yp \t\t : \t\t copy path (single line)
            yP \t\t : \t\t copy path (verbose)
            E \t\t : \t\t extract
            o \t\t : \t\t mimeopen
            O \t\t : \t\t mimeopen --ask
            F \t\t : \t\t fix biblatex
            mkd \t : \t\t mkdir
            <ctrl>+n \t : \t\t mkdir
            <ctrl>+x \t : \t\t make executable

        selection

            <space> \t : \t\t select
            v \t\t : \t\t toggle selection
            <esc> \t : \t\t clear all
            uu \t\t : \t\t clear all
            uy \t\t : \t\t clear yank
            ud \t\t : \t\t clear cut
            us \t\t : \t\t clear selection

            f \t\t : \t\t fuzzy finder
            F \t\t : \t\t fuzzy finder --> vim

        file info

            zh \t\t : \t\t show hidden files
            zr \t\t : \t\t reverse!
            zn \t\t : \t\t info
            zs \t\t : \t\t info size
            zt \t\t : \t\t info time
            za \t\t : \t\t info size:time
	
        sort

            sn \t\t : \t\t sort by natural\t ++ info
            ss \t\t : \t\t sort by size \t\t ++ info size
            st \t\t : \t\t sort by time \t\t ++ info time
            sa \t\t : \t\t sort by atime \t\t ++ info atime
            sc \t\t : \t\t sort by ctime \t\t ++ info ctime
            se \t\t : \t\t sort by ext \t\t ++ info 
            
            q \t\t : \t\t quit
            <ctrl>+r \t : \t\t reload client
" | \
yad --title="lf-help" --no-buttons --close-on-unfocus --text-info --back=#282c34 --fore=#46d9ff --geometry=960x800
