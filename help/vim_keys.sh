#!/usr/bin/env bash
set -euo pipefail

sed -n '/MAPPINGS/,/END_MAPPINGS/p' ~/.vimrc | \
    grep -e 'autocmd FileType vim' \
    -e 'autocmd FileType tex' \
    -e 'KB_GROUP' | \
    grep -v '\" autocmd' | \
    sed -e 's/^[ \t]*//' \
    -e 's/, (/(/' \
    -e 's/\[ (/(/' \
    -e 's/autocmd FileType vim /\n/' \
    -e 's/autocmd FileType tex /\n/' \
	-e 's/oremap /\ /' \
	-e 's/" KB_GROUP /\n/' \
    -e 's/", /"\t: /' | \
    yad --title="hotkeys" --no-buttons --close-on-unfocus --text-info --back=#282c34 --fore=#46d9ff --geometry=1200x800
