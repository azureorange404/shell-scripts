#!/bin/bash

printf "\
\n\n\n
	Xmonad

		Mod-Ctrl-r			:		recompile xmonad
		Mod-Shift-r			:		restart xmonad
		
	Get help

		Mod-<F1>			:		xmonad keybindings
		Mod-<F2>			:		vim keybindings
		Mod-<F3>			:		lf keybindings
		Mod-<F4>  			:  		phsg curriculum

	Run Prompt

		Mod-Shift-<Return>		:  		Dmenu
		Mod-r  				:  		Dmenu
		Mod-t				:		Tree launcher
		Mod-g g				:		Grid Launcher

	Other Dmenu Prompts

		Mod-p h   			:  		allows access to all dmscripts
		Mod-p s	  			:  		choose an ambient background
 		Mod-p c	  			:  		pick color from our scheme
 		Mod-p k	  			:  		kill processes
 		Mod-p n	  			:  		store one-line notes and copy them
 		Mod-p p	  			:  		passmenu
 		Mod-p q	  			:  		logout menu
 		Mod-p r	  			:  		online radio
 		Mod-p y	  			:  		search various search engines

	Useful programs to have a keybinding for launch

		Mod-<Return>  			: 	 	terminal
 		Mod-b b  			:  		brave
 		Mod-b o  			:  		opera
 		Mod-b f  			:  		firefox
 
		Mod-m m  			:  		thunderbird
		Mod-m t				:		teams

 		Mod-e e  			:  		lf
 		Mod-e t  			:  		thunar

		Mod-v  				:  		keepassxc
 		Mod-Ctrl-w  			:  		change wallpaper
 		Mod-l  				:  		lock screen
 		Mod-Shift-l  			:  		dm-logout

	Scratchpads

		Mod-s t  :  terminal
 		Mod-s c  :  calculator
 		Mod-s p  :  calendar
 		Mod-s n  :  notes
 		Mod-s s  :  spotify

 		Mod-s q  :  signal
 		Mod-s w  :  whatsapp
 		Mod-s e  :  slack

	Kill windows

		Mod-q  				:  		Kill the currently focused client

	Workspaces

		Mod-.  				:  		switch focus to next monitor
 		Mod-,  				:  		switch focus to prev monitor
 		Mod-Shift-<Right>  		:  		shift focused window to next ws
 		Mod-Shift-<Left>  		:  		shift focused window to prev ws
 		Mod-<Left>  			:  		move to prev ws
 		Mod-<Right>  			:  		move to next ws

	Floating windows

		Mod-Ctrl-f  :  Toggles my 'floats' layout
 		Mod-Ctrl-t  :  Push floating window back to tile
 		Mod-Shift-t  :  Push ALL floating windows to tile

	Increase/decrease spacing (gaps)
 		C-M1-j  :  Decrease window spacing
 		C-M1-k  :  Increase window spacing
 		C-M1-h  :  Decrease screen spacing
 		C-M1-l  :  Increase screen spacing

	Grid Select (CTR-g followed by a key)
 		Mod-g g  :  grid select scratchpads
 		Mod-g t  :  goto selected window
 		Mod-g b  :  bring selected window

	Windows navigation
 		Mod-j  :  Move focus to the next window
 		Mod-<Down>  :  Move focus to the next window
 		Mod-k  :  Move focus to the prev window
 		Mod-<Up>  :  Move focus to the prev window
 		Mod-Shift-m  :  Swap the focused window and the master window
 		Mod-Shift-j  :  Swap focused window with next window
 		Mod-Shift-k  :  Swap focused window with prev window
 		Mod-<Backspace>  :  Moves focused window to master, others maintain order
 		Mod-Shift-<Tab>  :  Rotate all windows except master and keep focus in place
 		Mod-Ctrl-<Tab>  :  Rotate all the windows in the current stack

	Layouts
 		Mod-<Tab>  :  Switch to next layout
 		Mod-<Space>  :  Toggles noborder/full

	Increase/decrease windows in the master pane or the stack

		Mod-Shift-<Up>  :  Increase # of clients master pane
 		Mod-Shift-<Down>  :  Decrease # of clients master pane
 		Mod-Ctrl-<Up>  :  Increase # of windows
 		Mod-Ctrl-<Down>  :  Decrease # of windows

	Window resizing

		Mod-Ctrl-h  :  Shrink horiz window width
 		Mod-Ctrl-l  :  Expand horiz window width
 		Mod-Ctrl-j  :  Shrink vert window width
 		Mod-Ctrl-k  :  Expand vert window width

	Sublayouts

	Controls for mocp music player (SUPER-u followed by a key)

	Emacs (SUPER-e followed by a key)

	Multimedia Keys
 		<XF86AudioPlay>  :  play
 		<XF86AudioPrev>  :  prev 
 		<XF86AudioNext>  :  next
 		<XF86AudioMute>  :  mute
 		<XF86AudioLowerVolume>  :  lower volume
 		<XF86AudioRaiseVolume>  :  raise volume
 		<XF86HomePage>  :  brave 'duckduckgo.com'
 		Mod-<KP_Divide>  :  dm-websearch
 		<XF86MonBrightnessUp>  :  brightness up
 		<XF86MonBrightnessDown>  :  brightness down
 		<Print>  :  screenshot
 		Mod-Shift-<Print>  :  region screenshot
 		Mod-M1-<Print>  :  delayed screenshot

		
" | \
yad --title="lf-help" --no-buttons --close-on-unfocus --text-info --back=#282c34 --fore=#46d9ff --geometry=960x800
