#!/bin/bash

printf "\
		\n\n\n
        Keybindings for symbols

            Press <Ctrl>+<Shift>+U, then the Unicode code, then <Enter>

            Unicode code \t Name

            2013 \t–\t Halbgeviertstrich (Gedankenstrich) - en dash
            2014 \t—\t Geviertstrich - em dash

            2026 \t…\t Auslassungspunkte - horizontal ellipsis
            2027 \t‧\t Silbentrennungspunkt - hyphenation point

            203c \t‼\t Doppelausrufezeichen - double exclamation mark
            2047 \t⁇\t Doppelfragezeichen - double question mark
            2048 \t⁈\t Frage-, Ausrufezeichen - question exclamation mark
            2049 \t⁉\t Ausrufe-, Fragezeichen - exclamation question mark

            2022 \t•\t Aufzählungszeichen - bullet
            2023 \t‣\t dreieckiges Aufzählungszeichen - triangular bullet
            2043 \t⁃\t Bindestrichaufzählungszeichen - hyphen bullet

            2030 \t‰\t Promillezeichen - per mille sign
            2031 \t‱\t Permyriadzeichen - per ten thousand sign
            2032 \t′\t Minutenzeichen - prime
            2033 \t″\t Sekundenzeichen - double prime 
            2044 \t⁄\t Bruchstrich - fraction slash
" | \
yad --title="lf-help" --no-buttons --close-on-unfocus --text-info --back=#282c34 --fore=#46d9ff --geometry=800x800
