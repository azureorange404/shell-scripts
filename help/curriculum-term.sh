#!/bin/sh

# SCRIPT_DIR=$( cd -- "$( dirname -- "${DASH_SOURCE[0]}" )" &> /dev/null && pwd )
CURRICULUM="$HOME/.config/shell/scripts/help/curriculum-list"

nextworkday () {

    if [ "$(date +%w)" -lt 5 ];
    then
        date +%a -d "next day"
        return
    fi
    echo Mon

}

gettime () {
    
    if [ "$(date +%H%M)" -lt 200 ];
    then
        date +%H%M
        return
    fi
    date +%H%M -d "- 2 hours"
}

time=$(gettime)
day=$(date +%a)

schedule=$(grep -E "^$day [0-9]{4}" "$CURRICULUM" | awk -v x="$time" '$2 > x' | sed -e s/"$day"//)

if [ "$schedule" = "" ]; then
    day=$(nextworkday)
    schedule=$(grep -E "^$day [0-9]{4}" "$CURRICULUM" | sed -e s/"$day"\ //)
fi

st -t float -g 60x$(($(printf "%s" "$schedule" | wc -l) + 3)) -f "FiraCodeNerdFont:pixelsize=20:antialias=true:autohint=true" -e bash -c "echo '$day' && read -p '$schedule    '"
