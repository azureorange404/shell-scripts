#!/bin/bash

printf "\
\n\n\n
	Newm

		Mod-Shift-r			:		newm update config
		
	Get help

		Mod-<F1>			:		newm keybindings
		Mod-<F2>			:		vim keybindings
		Mod-<F3>			:		lf keybindings
		Mod-<F4>  			:  		phsg curriculum

	Run Prompt

		Mod-Shift-<Return>		:  		Wofi

	Other Dmenu Prompts

 		Mod-p k	  			:  		kill processes
        Mod-p n	  			:  		store notes, copy and edit them (joplin)
        Mod-p c	  			:  		store events, copy and edit them (khal)
 		Mod-p p	  			:  		passmenu

 		Mod-Shift-l  			:  		logout menu

	Useful programs to have a keybinding for launch

		Mod-<Return>  			: 	 	terminal
 		Mod-b b  			:  		qutebrowser
 		Mod-b o  			:  		brave
 		Mod-b f  			:  		firefox
 
		Mod-m m  			:  		thunderbird
		Mod-m t				:		teams
		Mod-m w				:		whatsapp
		Mod-m s				:		slack
		Mod-m q				:		signal

 		Mod-e e  			:  		lf
 		Mod-e p  			:  		pcmanfm

	Kill windows

		Mod-q  				:  		Kill the currently focused client

	Floating windows

		Mod-v  :  Toggles float

	Increase/decrease window size

 		Mod-C-Right         :       Increase horizontal window size
 		Mod-C-Down          :       Increase vertical window size
 		Mod-C-Left          :       Decrease horizontal window size
 		Mod-C-Up            :       Decrease vertical window size

	Windows navigation

 		Mod-Right           :       Move focus right
 		Mod-Down            :       Move focus down
 		Mod-Left            :       Move focus left
 		Mod-Up              :       Move focus up

 		Mod-S-Right         :       Move window right
 		Mod-S-Down          :       Move window down
 		Mod-S-Left          :       Move window left
 		Mod-S-Up            :       Move window up

	Multimedia Keys
 		<XF86AudioPlay>  :  play
 		<XF86AudioPrev>  :  prev 
 		<XF86AudioNext>  :  next
 		<XF86AudioMute>  :  mute
 		<XF86AudioLowerVolume>  :  lower volume
 		<XF86AudioRaiseVolume>  :  raise volume
 		<XF86HomePage>  :  brave 'duckduckgo.com'
 		Mod-<KP_Divide>  :  dm-websearch
 		<XF86MonBrightnessUp>  :  brightness up
 		<XF86MonBrightnessDown>  :  brightness down
 		<Print>  :  screenshot
 		Mod-Shift-<Print>  :  region screenshot
 		Mod-M1-<Print>  :  delayed screenshot

		
" | \
yad --title="lf-help" --no-buttons --close-on-unfocus --text-info --back=#282c34 --fore=#46d9ff --geometry=960x800
